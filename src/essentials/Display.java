package essentials;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Display extends GameElement {

	public static final Font FONT = new Font("Bahnschrift", 0, 20);

	private static Display instance;

	private int UPS;
	private int FPS;

	private Display() {}

	@Override
	public void tick() {

	}

	@Override
	public void render(Graphics g) {
		this.drawFPS(g);
	}

	private void drawFPS(Graphics g) {
		g.setFont(FONT);
		g.setColor(Color.WHITE);
		String ticks = String.valueOf(this.UPS);
		String fps = String.valueOf(this.FPS);
		int width = Window.getWidth();
		g.drawString(ticks, width - 80 - g.getFontMetrics().stringWidth(ticks), 20);
		g.drawString("ticks", width - 70, 20);
		g.drawString(fps, width - 80 - g.getFontMetrics().stringWidth(fps), 45);
		g.drawString("fps", width - 70, 45);
	}

	public int getUPS() { return this.UPS; }

	public void setUPS(int ups) { this.UPS = ups; }

	public int getFPS() { return this.FPS; }

	public void setFPS(int fps) { this.FPS = fps; }

	public static Display getInstance() {
		if (instance == null) instance = new Display();
		return instance;
	}
}
