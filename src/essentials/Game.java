package essentials;

public class Game {

	private static Window window;
	private static Handler handler;
	private static Display display;

	public Game() {
		ImageLoader.load();
		window = Window.getInstance();
		handler = Handler.getInstance();
		display = Display.getInstance();
		new Thread(window).start();
	}

	public static void tick() {
		handler.tick();
		display.tick();
	}

	public static Window getWindow() { return window; }

	public static Handler getHandler() { return handler; }

	public static Display getDisplay() { return display; }

	public static void main(String[] args) {
		new Game();
	}
}
