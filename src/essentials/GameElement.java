package essentials;

import java.awt.Graphics;

public abstract class GameElement {

	public abstract void tick();

	public abstract void render(Graphics g);
}
