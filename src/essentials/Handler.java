package essentials;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import objects.GameObject;

public class Handler extends GameElement {

	private static Handler instance;

	private List<GameObject> objects;

	private Handler() {
		this.objects = new ArrayList<>();
	}

	@Override
	public void tick() {
		synchronized (this.objects) {
			for (int i = 0; i < this.objects.size(); i++) {
				GameObject tempObject = this.objects.get(i);
				tempObject.tick();
			}
		}
	}

	@Override
	public void render(Graphics g) {
		synchronized (this.objects) {
			for (int i = 0; i < this.objects.size(); i++) {
				GameObject tempObject = this.objects.get(i);
				tempObject.render(g);
			}
		}
	}

	public void clear() {
		synchronized (this.objects) {
			this.objects.clear();
		}
	}

	public void addObject(GameObject obj) {
		synchronized (this.objects) {
			this.objects.add(obj);
		}
	}

	public void removeObject(GameObject obj) {
		synchronized (this.objects) {
			this.objects.remove(obj);
		}
	}

	public static Handler getInstance() {
		if (instance == null) instance = new Handler();
		return instance;
	}
}