package essentials;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Window implements Runnable {

	private static final int WIDTH = 1024;
	private static final int HEIGHT = 768;
	private static final String TITLE = "Game";

	private static Window instance;

	private JFrame frame;
	private Canvas canvas;
	private boolean running;

	private Window() {
		this.frame = new JFrame(TITLE);
		this.frame.setSize(WIDTH, HEIGHT);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setResizable(false);
		this.frame.setLocationRelativeTo(null);

		this.canvas = new Canvas();
		this.frame.add(this.canvas);
		this.frame.setVisible(true);
		this.frame.requestFocus();

		this.running = true;
	}

	public JFrame getFrame() { return this.frame; }

	@Override
	public void run() {
		this.canvas.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double delta = 0;
		int frames = 0, ticks = 0;
		long timer = System.currentTimeMillis();
		while (this.running) {
			long now = System.nanoTime();
			delta += (now - lastTime) * amountOfTicks / 1000000000;
			lastTime = now;
			while (delta >= 1) {
				Game.tick();
				ticks++;
				delta--;
			}
			if (this.running) {
				Window.getInstance().render();
				frames++;
			}
			if (System.currentTimeMillis() - timer > 1000) {
				Display.getInstance().setUPS(ticks);
				Display.getInstance().setFPS(frames);
				frames = 0;
				ticks = 0;
				timer += 1000;
			}
		}
		this.stop();

	}

	private void stop() {
		//TODO
	}

	private void render() {
		BufferStrategy bs = this.canvas.getBufferStrategy();
		if (bs == null) {
			this.canvas.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();

		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.setColor(Color.black);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		Game.getHandler().render(g);
		Game.getDisplay().render(g);

		g.dispose();
		bs.show();
	}

	public static int getWidth() { return WIDTH; }

	public static int getHeight() { return HEIGHT; }

	public static Window getInstance() {
		if (instance == null) instance = new Window();
		return instance;
	}
}
