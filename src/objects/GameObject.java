package objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import essentials.GameElement;

public abstract class GameObject extends GameElement {

	protected double x, y;
	protected double velX, velY;
	protected BufferedImage img;
	protected int sizeX;
	protected int sizeY;

	public GameObject(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public abstract void tick();

	@Override
	public abstract void render(Graphics g);

	public void setX(int x) { this.x = x; }

	public void setY(int y) { this.y = y; }

	public double getX() { return this.x; }

	public double getY() { return this.y; }

	public void setVelX(double velX) { this.velX = velX; }

	public void setVelY(double velY) { this.velY = velY; }

	public double getVelX() { return this.velX; }

	public double getVelY() { return this.velY; }

}
